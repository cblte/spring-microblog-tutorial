# Spring Microblog Docs

Tutorial files for the Spring-Microblog project. The page can be found here <https://cblte.codeberg.page/spring-microblog-docs/>

## MKDocs Plugins used

- **search**
- **glightbox** for a nice lightbox to zoom pictures.

## Generation and deployment

1. Create a Shell Script File:

    Open a text editor and create a new file, for example, deploy_mkdocs.sh.

2. Write the Script:

    Add the following content to the file:

    ``` shell
    #!/bin/bash
    
    # Exit if any command fails
    set -e
    
    # Build the MkDocs site
    mkdocs build -c -d site
    
    # Delete the pages branch if it exists
    git branch -D pages || true
    
    # Create a new orphan pages branch
    git switch --orphan pages
    
    # Copy the site content to the current directory
    cp -a site/* .
    
    # Add all files to Git
    git add *
    
    # Commit the changes
    git commit -m "Committing new site updates"
    
    # Force push to the pages branch
    git push --force --set-upstream origin pages
    
    # Switch back to the main branch
    git checkout main
    
    # Delete the pages branch
    git branch -D pages
    ```

3. Make the Script Executable:

    Save the file and then make it executable by running the following command in the terminal:

    ``` shell
    chmod +x deploy_mkdocs.sh
    ```

4. Run the Script:

    Now, you can run the script whenever you want to deploy your MkDocs site:

    ``` shell
    ./deploy_site.sh
    ```

This script will execute the steps you listed: building the MkDocs site, managing Git branches, copying files, committing, and pushing to the remote repository. The `set -e` command at the beginning of the script causes the script to exit if any command fails, which is useful for catching errors early.

Remember to test the script in a safe environment first, especially because it involves force-pushing to a remote repository. Also, ensure that the path to the MkDocs build directory (`site/*` in the script) is correct for your setup.
