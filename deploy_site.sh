#!/bin/bash

# Exit if any command fails
set -e

# Build the MkDocs site
mkdocs build -c

# Delete the pages branch if it exists
git branch -D pages || true

# Create a new orphan pages branch
git switch --orphan pages

# Copy the site content to the current directory
cp -a site/* .

# Add all files to Git
git add *

# Commit the changes
git commit -m "Committing new site updates"

# Force push to the pages branch
git push --force --set-upstream origin pages

# Switch back to the main branch
git checkout main

# Delete the pages branch
git branch -D pages