# Chapter 01: Hello World

## Spring Boot Unveiled

Spring Boot stands out as a leading framework for constructing Java-based web applications and microservices. A standout feature of Spring Boot lies in its convention-over-configuration philosophy, empowering developers to forge robust applications with minimal setup and a reduction in boilerplate code. In this blog post, we focus shifts to unravele the entry point of a Spring Boot application and understanding how it propels the application into action.

## Exploring the Java Spring Boot Project Setup

Embarking on a journey of practical application, this tutorial will guide you through the creation of a web application using Java and the Spring Boot 3 Framework. The first chapter sets the stage by gaining knowledge on setting up a Java Spring Boot project within the Spring Tool Suite—an IDE rooted in Eclipse.

## Code at Your Fingertips

All code examples featured in this tutorial are conveniently accessible in a git repository. While downloading the code provides a shortcut, I strongly recommend immersing yourself in the learning experience by typing the code manually. This hands-on approach ensures a deeper understanding of the intricacies involved in crafting Spring Boot applications.

## Repository links for this chapter:

[Browse](https://codeberg.org/cblte/spring-microblog/src/tag/v0.1) the source code, download a [zip-file](https://codeberg.org/cblte/spring-microblog/archive/v0.1.zip), view the [differences](https://codeberg.org/cblte/spring-microblog/compare/v0.0...v0.1)

## Installing Java

If you don't have Java installed on your computer, there are several ways to set it up. For MacOS or Linux users, I recommend exploring [SDKMAN!](https://sdkman.io/) as a convenient tool for managing Java installations. Windows users can also use SDKMAN by installing it through the Git Bash terminal. Alternatively, you may opt for the [Community Edition of Chocolatey Windows Package Manager](https://community.chocolatey.org/), which is akin to [Homebrew](https://brew.sh) for MacOS, providing an easy way to manage packages on Windows.

If you prefer not to use a download or package manager, you can manually download the latest stable release for your operating system from [Adoptium](https://adoptium.net/en-GB/temurin/releases/).

!!! Info
    For this tutorial, we will be utilizing [Java Temurin 21 from Adoptium](https://adoptium.net/de/temurin/releases/?package=jdk&version=21).

To verify the correctness of your Java installation, open a terminal and type the following commands: `java -version` and `javac -version`. These commands will display the installed Java Development Kit (JDK) version. Additionally, modern Java installations come with a Java shell, accessible by running the `jshell` command. When you execute the `javac -version` command, the output should resemble the following:

```bash
❯ javac -version
javac 21.0.1

~
❯
```

If you run the `jshell` command, the shell will be ready to accept Java code or commands. Experiment with it by typing `/help intro` for a small introduction or exit by typing `/exit` or pressing `CTRL+D`.

## Setting up the Development Environment

The next step in your journey is to establish a proper development environment. While a simple text editor can be used for Java code, we recommend the convenience of Spring Tool Suite (STS), an Integrated Development Environment (IDE) based on the popular Eclipse IDE. Navigate to [Spring Tool Suite](https://spring.io/guides/gs/sts) and download the latest version compatible with your operating system.

For terminal enthusiasts, you can use your preferred package manager to download STS. There are packages available for [Homebrew](https://formulae.brew.sh/cask/springtoolsuite) and [Chocolatey](https://community.chocolatey.org/packages/SpringToolSuite).

If Eclipse isn't your cup of tea, the Spring plugins for alternatives like [IntelliJ IDEA](https://spring.io/guides/gs/intellij-idea/) or [VSCode](https://spring.io/guides/gs/guides-with-vscode/) are viable options.

After downloading STS, unpack the archive and run it by executing the `spring-tool-suite` executable. If installed via a package manager, you should find it in your applications listing.

!!! Tip "Good To Know"
    Spring Tool Suite comes bundled with Maven, eliminating the need for a separate installation.

## Setting up the Spring Boot Project

Once your IDE is installed, we can setup a basic "Hello World" Spring Boot. This can be done in two ways.

You can use this [pre-initialized project](https://start.spring.io/#!type=maven-project&language=java&platformVersion=3.2.2&packaging=jar&jvmVersion=21&groupId=com.example&artifactId=springmicroblog&name=springmicroblog&description=Spring%20Microblog&packageName=com.example.springmicroblog&dependencies=web) and click the Generate button to download a ZIP file. This project is configured to fit this first step of the tutorial.

Another way is to manually initialize the project.

1. Navigating to <https://start.spring.io/>. This service pulls in all the dependencies you need for an application and does most of the setup for you.
2. On the *Spring Initializr* website choose **Maven** and **Java** as the language.
3. Select latest stable version of Spring Boot. At the moment of writing this is **3.2.2**.
4. Enter all relevant Project Metadata:
    - group: `com.example`
    - artifact: `springmicroblog`
    - name: `springmicroblog`
    - description: `Spring Microblog`
    - package name: `com.example.springmicroblog`
5. For Packaging select **Jar**.
6. For the Java version select **21**.
7. Click **Dependencies** and select **Spring Web**.
8. Click **Generate**
9. Download the resulting ZIP file, which is an archive of a web application that is configured with your choices.
10. Extract the ZIP and import the project as an **Existing Maven Project** into STS

All of the above steps can also be done within the Spring Tool Suite by navigation to **File** -> **New** -> **Spring Starter Project**.

Either you created the project within the Spring Tool Suite or used the Spring.io-Initilizer and imported the extracted project, your IDE should look similar to this:

![Screenshot of the Spring Tool Suite after our project has been loaded](images/ch01-setup-spring-boot-project-sts-screenshot.png) 

*Spring Tool Suite with the imported Spring Boot project*

## An Brief Introduction of the Generated Project

You can, of course, run the application as it is, but initially, you won't see much output. So, let's take a moment to explore some of the existing components before adding code to display a delightful "Hello World" on the screen.

The basic project comprises several files that we'll quickly go through since we'll be introducing additional elements throughout the tutorial.

If you want to try anyway, with your project loaded, locate the `SpringMicroblogApplication` class in the `src/main/java/com.example.springmicroblog` package. Right-click on this class, and from the context menu, choose `Run As` -> `Spring Boot App`.

Now, your Spring Boot application is up and running! Keep an eye on the console for any output, and in subsequent sections, we'll explore how to enhance this basic setup and bring more life to your Spring Boot project.

To stop the application, you can click the red square button in the Console tab, often labeled "Terminate" or "Stop the selected launch," which halts the running Spring Boot process.

Alternatively, you can run the application from the command line by executing the command `mvn spring-boot:run` in a terminal. Once the application is running, you can use `Ctrl + C` (on Windows or macOS) to stop the application. Keep in mind that after pressing `Ctrl + C`, it might take a moment as the application attempts to shut down gracefully.

!!! Tip "Using Spring Boot DevTools for Efficient Development"
    By simply adding this dependency to your Maven project, you unlock features such as automatic restart and live reload. This means that any changes in your codebase are immediately reflected in your running application, saving you the time and hassle of manually restarting your server.

    ```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
        </dependency>
    ```

### SpringMicroblogApplication.java

First we have the `SpringMicroblogApplication.java` class file:

``` java
package com.example.springmicroblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMicroblogApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMicroblogApplication.class, args);
    }

}
```

**Import Statements:**

The import statements bring in classes from the Spring Boot framework. `SpringApplication` is a class that bootstraps a Spring application, and `@SpringBootApplication` is an annotation that combines multiple annotations to configure the application.

**@SpringBootApplication Annotation:**

This annotation is a meta-annotation that combines three other annotations:

- @Configuration: Indicates that the class can be used as a source of bean definitions.
- @EnableAutoConfiguration: Enables Spring Boot's auto-configuration feature, which automatically configures the application based on its dependencies.
- @ComponentScan: Instructs Spring to scan and discover beans (components, services, etc.) in the specified package (com.example.springmicroblog in this case).

**Main Method:**

```java
public static void main(String[] args) {
    SpringApplication.run(SpringMicroblogApplication.class, args);
}
```

The main method serves as the entry point for the application. It calls `SpringApplication.run` to bootstrap the Spring application. This method takes two parameters:

- The class that contains the main method (`SpringMicroblogApplication.class` in this case).
- The command-line arguments passed to the application.

### application.properties

In the application.properties file, you can configure various properties for your Spring Boot application. Let's add some basic configurations and enable debugging for the web during development.

Update your `application.properties` file with the following content:

```properties
# Set the server port (default is 8080)
server.port=8080

# Enable debug logging for the web
logging.level.org.springframework.web=DEBUG
```

- `server.port`: This property sets the port on which the embedded server (Tomcat, by default) will run. In this example, it's set to 8080. You can change it to a different port if needed.
- `logging.level.org.springframework.web`: This property sets the logging level for the `org.springframework.web` package to DEBUG. This will enable more detailed logging for web-related components during development.

You can add or modify properties in this file based on your application's requirements and we will add more in the future. These are just some basic configurations to get you started. Feel free to adjust them according to your needs. The documentation of Spring contains a very longlist [Common Application Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html).

Remember to keep the `application.properties` file in the `src/main/resources` directory of your project so that Spring Boot can pick it up automatically. If you're using `application.yml` instead of `.properties`, the same configurations can be applied with appropriate YAML syntax.

!!! note
    In this tutorial we will be using the .properties file format.

### pom.xml file

Since we are using Maven for your Dependency Management the `pom.xml` file is a crucial configuration file in Maven projects, including Spring Boot applications. It stands for "Project Object Model" and defines the project's structure, dependencies, plugins, and build settings. Let's break down the key sections:

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <!-- Highlighted: The version of the Spring Boot Starter Parent -->
    <version>3.2.2</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

The `parent` tag defines the parent project, inheriting its configuration. Here, it inherits from the Spring Boot Starter Parent, version 3.2.2.

```xml
<groupId>com.example</groupId>
<artifactId>spring-microblog</artifactId>
<version>0.0.1-SNAPSHOT</version>
<name>spring-microblog</name>
<description>Spring Microblog</description>
```

`<groupId>`, `<artifactId>`, `<version>`, `<name>`, and `<description>` specify fundamental project details.

```xml
<dependencies>
    <!-- Dependencies section lists project dependencies -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
    </dependency>
    <!-- Highlighted: New dependency for testing -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>
```

`<dependencies>` lists project dependencies. Here, it includes Spring Boot Starter Web for web applications, Spring Boot DevTools and a testing dependency.

```xml
<build>
    <plugins>
        <!-- Maven plugin for Spring Boot -->
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

`<build>` and `<plugins>` define the build process. In this case, it includes the Spring Boot Maven plugin, simplifying the build and run processes for Spring Boot applications.

This `pom.xml` file simplifies project management, making it easy to handle dependencies, build processes, and other configurations in a standardized manner.

While we make progress in developing the microblog, expanding the range of dependencies in the `pom.xml` file is a pivotal step toward enriching the features and capabilities of the Spring Boot project. Each added dependency brings specific functionalities and tools that contribute to a more robust and versatile application.

### SpringMicroblogApplicationTests.java file

The basic Spring Boot project also includes a dedicated test class. The default generated test contains a fundamental check to ensure that the application starts up successfully. Noteworthy at this stage is the presence of the `@SpringBootTest` annotation. Placed at the class level, it configures the tests to operate within the context of a Spring Boot application. In simpler terms, it mimics the application's setup during testing, providing a reliable mechanism to validate that the core functionality is intact.

Understanding the `@SpringBootTest` annotation is a key step in the testing journey. It enables us to seamlessly integrate testing into the Spring Boot environment, ensuring that your application behaves as expected. As we progress through this tutorial, we might delve into more sophisticated test scenarios, unraveling some aspects of Spring Boot testing.

## A 'Hello World' Spring Application

Finally we have come to the point, where came to the point to introduce a simple controller to display a "Hello World" message.

**Follow these steps:**

Create a new Java class in the `com.example.springmicroblog` package, and name it `HelloController`:

```java
package com.example.springmicroblog;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

   @GetMapping("/hello")
   public String hello() {
       return "Hello World from Spring Microblog!";
   }
}
```

The `HelloController` class is annotated with the `@RestController` annotation, indicating that it's is a specialized version of the `@Controller` annotation that is used to define RESTful web services.

In the context of a Spring Boot application, this annotation is used at the class level to indicate that this class is a REST controller. It combines the `@Controller` and `@ResponseBody` annotations. The `@Controller` annotation marks a class as a Spring MVC controller, and `@ResponseBody` indicates that the return value of a method should be directly written to the response body.

??? Hint "Click to read more about the `@Controller` annotation."

    In a Spring MVC application, when a controller method is executed in response to a web request, the method typically returns a value. The `@ResponseBody` annotation in Spring is used to indicate that the return value of the method should be directly written to the body of the HTTP response instead of being interpreted as a view name (as it would be in the absence of `@ResponseBody`).

    Let's look at an example without `@ResponseBody`:

    ```java
    @Controller
    public class MyController {

        @GetMapping("/greet")
        public String greet() {
            return "Hello World!";
        }
    }
    ```

    In this case, if the `greet()` method is invoked in response to a request, the string "Hello World!" is not directly written to the response body. Instead, it's treated as the name of a view, and Spring would look for a view with that name to render.

    Now, let's modify the example using `@ResponseBody`:

    ```java
    @Controller
    public class MyController {

        @GetMapping("/greet")
        @ResponseBody
        public String greet() {
            return "Hello World!";
        }
    }
    ```

    With the addition of `@ResponseBody`, the string "Hello World!" returned by the `greet()` method is directly written to the body of the HTTP response. This is useful when you want the controller method to produce raw data (like JSON, XML, or plain text) directly as the response, rather than relying on a view resolver to render a view. An good example for a renderer is a template engine like Mustache or Thymeleaf.

    In the case of `@RestController`, it combines `@Controller` and `@ResponseBody`, so every method is implicitly treated as if it has `@ResponseBody`. This makes it convenient for building RESTful services where the response is often raw data rather than a view.

The `hello()` method, annotated with `@GetMapping("/hello")`, handles HTTP GET requests for the `/hello` endpoint. When a request is made to "/hello," this method is invoked, and it returns the string "Hello World from Spring Microblog!".

**Now, restart your Spring Boot application**, or wait for it to be restarted when you have added the DevTools to the `pom.xml` file.

If you are using Spring Boot Tools Suite (STS), you could also stop the application by clicking the red square button in the Console tab, and then rerun it.

If you are running from the command line, use `Ctrl + C` (Windows or macOS) to stop the application and then run `mvn spring-boot:run` again.

Once the application is up and running, open your web browser and navigate to <http://localhost:8080/hello>. You should see the "Hello World from Spring Microblog" message.

When take a look at the console output when you access the `/hello` endpoint of your Microblog application, you may observe detailed logs in the console. These logs provide valuable insights into the internal workings of the application during the HTTP request process. Below is an example snippet of the logs:

```log
2024-01-22T22:53:10.366+01:00 DEBUG 63268 --- [nio-8080-exec-3] o.s.web.servlet.DispatcherServlet       : GET "/hello", parameters={}
2024-01-22T22:53:10.366+01:00 DEBUG 63268 --- [nio-8080-exec-3] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped to com.example.springmicroblog.HelloController#hello()
2024-01-22T22:53:10.371+01:00 DEBUG 63268 --- [nio-8080-exec-3] m.m.a.RequestResponseBodyMethodProcessor : Using 'text/html', given [text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8] and supported [text/plain, */*, application/json, application/*+json]
2024-01-22T22:53:10.371+01:00 DEBUG 63268 --- [nio-8080-exec-3] m.m.a.RequestResponseBodyMethodProcessor : Writing ["Hello World from Spring Microblog!"]
2024-01-22T22:53:10.372+01:00 DEBUG 63268 --- [nio-8080-exec-3] o.s.web.servlet.DispatcherServlet       : Completed 200 OK
```

In this log excerpt, each line represents a step in the request-handling process. From mapping the request to a specific method (`Mapped to com.example.springmicroblog.HelloController#hello()`) to processing and producing the response, these logs offer a detailed view of how your Spring Boot application handles the `/hello` endpoint.

## Congratulations

Congratulations! You've successfully added a Hello World controller to your Spring Boot application. This simple introduction demonstrates the basics of setting up a Spring Boot project with the help of Spring Initializr and running the project inside Spring Tool Suite. Additionally, we covered the handling of basic HTTP requests with Spring by adding a `@RestController` to your project.

In the upcoming chapters, we'll delve into more advanced features and functionalities to further enrich your Spring Boot project. We'll explore the integration of a template engine, delve into web forms, and take the exciting steps of pulling and storing data from a database, among other advanced topics.
