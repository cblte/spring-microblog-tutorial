# Welcome to the Spring Microblog Tutorial

Greetings! If you're on the hunt for a comprehensive tutorial to delve into the realms of the Java Spring Framework or master the art of crafting applications with Spring Boot, you've landed in the right place.

Feel free to browse the tutorial but I suggest you start at [Chapter 1](chapter-01-hello-world.md) to learn how to setup your environment and get a basic hello world example running. This will make sure that everything is setup and you can continue on your journey in building a small microblog application including, security, registration, multi-user, database, templating and themes.

Enjoy and have fun.

## Table Of Contents

- [Chapter 01: Hello, World!](chapter-01-hello-world.md)
- [Chapter 02: Templates](chapter-02-templates.md)
- Chapter 03: Web Forms
- Chapter 04: Database
- Chapter 05: User Logins
- Chapter 06: Profile Page and Avatars
- Chapter 07: Error Handling
- Chapter 08: Followers
- Chapter 09: Pagination
- Chapter 10: Email Support
- Chapter 11: Facelift (this article)
- Chapter 12: Dates and Times
- Chapter 13: I18n and L10n
- Chapter 14: Ajax
- Chapter 15: A Better Application Structure
- Chapter 16: Full-Text Search
- Chapter 17: Deployment on Linux
- Chapter 18: Deployment on Heroku
- Chapter 19: Deployment on Docker Containers
- Chapter 20: Some JavaScript Magic
- Chapter 21: User Notifications
- Chapter 22: Background Jobs
- Chapter 23: Application Programming Interfaces (APIs)

## A Big Thank You

This tutorial is based and heavily inspired by the Mega Flask Tutorial from Miguel Grinberg, which I used to learn about the Flask Python Framework. You can find the [Mega Flask Tutorial here](http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) and [the code repository here](https://github.com/miguelgrinberg/microblog) for all things code related to the Mega Flask Tutorial.

I want to extend a sincere thank you to Miguel Grinberg for his outstanding work on the Mega Flask Tutorial. His comprehensive and well-explained guide has been an invaluable resource for learning Flask, and I am truly grateful for the effort he put into sharing his knowledge with the community.
