# Chapter 02: Templates

Welcome to part 2 of the Java Spring Microblog tutorial. In this chapter we will discuss how to work with templates.

==:warning: INSERT TOC here==

After completing Chapter 01, you will have a straightforward yet operational web application structured as follows.

==:warning: INSERT Overview here==

To run your application you can use the Sprint Tool Suite (STS) built in *Run as Spring Boot Application* functionality, or you can run it from the applications folder via commandline by executing the command `./mvnw spring-boot:run`. This will start the built in Apache Tomcat webserver with your application, which you can then open in your web browser by going to the URL <http://localhost:8080>.

In this chapter we will continue to work on the same application and we are going to learn how to utilise templates to generate more complex web pages which contain more complex content and dynamic components. You can of course go back to [Chapter 01](chapter-01-hello-world.md#installing-java) to re-read how to setup the environment.

:::info
Repository links for this chapter are [Browse](https://codeberg.org/cblte/spring-microblog/src/tag/v0.2), [Zip](https://codeberg.org/cblte/spring-microblog/archive/v0.2.zip), [Diff](https://codeberg.org/cblte/spring-microblog/compare/v0.1...v0.2)
:::
